const express = require('express')
const bodyParser = require("body-parser");
const app = express()
app.use(bodyParser.urlencoded({ extended: true }));
const port = 3000
require('./config/database').connect()
const User = require('./model/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const jwt_secret = 'i_am_a_man'

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post("/register", async (req, res) => {
  const { first_name, last_name, email, password } = req.body;
  if (!(email && password && first_name && last_name)) {
    res.send("All input is required");
  }

  const oldUser = await User.findOne({email})
  if (oldUser) {
    return res.send("User Already Exist. Please Login");
  }

  encryptedPassword = await bcrypt.hash(password, 10);
  const user = await User.create({
    first_name,
    last_name,
    email: email.toLowerCase(),
    password: encryptedPassword,
  })

  const token = jwt.sign(
      { user_id: user._id, email },
        jwt_secret,
      {
        expiresIn: "2h",
      }
  )
  user.token = token
  // return new user
  res.json(user)

})

app.post("/login", async (req, res) => {
  const { email, password } = req.body
  if (!(email && password)) {
    res.send("All input is required")
  }
  const user = await User.findOne({ email });
  if (user && (await bcrypt.compare(password, user.password))) {
    // Create token
    const token = jwt.sign(
      { user_id: user._id, email },
        jwt_secret,
      {
        expiresIn: "2h",
      }
    )
    user.token = token
    res.json(user)
  }
  res.send("Invalid Credentials")
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
